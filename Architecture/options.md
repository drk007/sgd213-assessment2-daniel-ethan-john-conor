# Architecture Options
We are to come up with three different options on how we want the Architecture of the project to be.


## Option 1:
* Four main actors: Player, Enemy, Environmental Interactions, Sound.
* Sound is linked from what the player does to what the enemy can hear.
* Objects are directly linked to the player to let the engine know what object they are allowed to pick up. (Inventory)
* When interacting with the car, It will notify the player if they have the right amount of car parts. (inventory)

## Option 2:
* Three main actors: Player, Enemy, Environmental Interactions
* Components such as Mesh, rigid body, player camera and animator all link to 'Player'
* Similar components link to the 'Enemy'
* Environmental components range from, objects which also branches off into multiple subsections, fuse box, and interactions with the car.
* Collectable items are branched off into multiple sub-components
* Sound is linked from what the player does to what the enemy can hear.

## Option 3:
* Two main actors: Player and Enemy.
* Sound is a componant that is linked to both the player and the enemy.
* Sound can be activated by going over any invisible box collider
* Objects are linked to the player through a component which shows the player what objects they have obtained.