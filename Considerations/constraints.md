# Constraints

## What may delay or push this project back?
Being very ambitous can be a big issue due to our timeframe.
COVID still being around can delay the project.
Amount of hours we as a team are able to put in together.
When the project is due.
Other university assignments/priorities.
Public holidays placed on work days.
Equipment could malfunction randomly.
Internet could be of issue.
Injuries may occur to staff which may push back their tasks in order for them to recover.