# Risks

## What could put this Project at Risk?
Any constrains that occur could put the project at risk of being delayed or incomplete.
Not asking enough questions to the client.
Workload not being evenly spread.
Inability to complete tasks whether that means inexperience or unavalibility.
Not having the right equipment at hand (Unity engine unavalible).
Not knowing what to do.
A staff member injured/dying.