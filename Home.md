# SGD213 Client Requirements Specification 


## Introduction
* This wiki discusses the client requirements for the Forest Lurker project.
* This project is being completed by Tahste for Michael.
* The handover date for the project is the end of Task 3.
* Just a note, each team member added input for the Tech Document on a Google Doc then copied over to this Bitbucket.

## Table of Contents

These are the two main areas of the assessment.

[//]: # (You can link to other pages in your wiki, or you can keep it inline)
* [Proposal](Proposal/index.md)
* [Architecture](Architecture/index.md)

## Expanding the Project

* [Overview](Overview/index.md)
* [Constraints & Risks](Considerations/index.md)