# Target Audience

## Who would this game appeal to?
People who enjoy horror.
Audiences that enjoy get a thrill from being frightened.
People who are of the age 15 or older.
Users who are not hard of hearing as this is a sound heavy game.

## Simililar Games of Which The Target Audience Would Enjoy
Slender (all games).
Amnesia.
Outlast.

## Player Experience Goal (PXG)
Players to feel as if they are trapped in a dark forest and always feel like they are being followed.
