# About the Company.

## Company
We are a small, four man indie company based in Australia who are still going through University with a big passion for developing games.

## Staff
## Daniel Kohn
* Hello, I am Daniel Kohn and I am a part of the company, Tahste.
* I am currently going through a Bachelor of Game Design at The University of The Sunshine Coast. 
* I also have a Certificate III in Information and Digital Technology. 
* I have a big passion for entertaining people through creating games and streaming to Twitch.
* I grew up all along the Australian east coast due to my father being in the Army. Mainly stayed in the North Brisbane area though.
* Gaming has always been a big part of my growing up, started when I was 2 when I was playing on the PS1 and then moved on to the Original Xbox and so on.

## Ethan Mott
* Hi, my name is Ethan Mott and I work in the company Tahste.
* I am currently studying at University of the Sunshine Coast with the aim of going through a Bachelor of Game Design. 
* My whole life I've been obsessed with making games. 
* I would make board games as a kid and at one point made a card game that I distributed and sold at my school for money. 
* At this stage I'm looking forward to making my childhood dreams a reality in the digital world.

## John Macale
* Hello, My name is John Macale, and I am involved with the indie company Tahste. 
* I am currently studying at the University of the Sunshine Coast, taking the Bachelor of Design (Game Design). 
* My very first game console was an Xbox Original, where I was first introduced to gaming. 
* I am interested in furthering my knowledge on the creation and understanding of game development.

## Conor Bradfield
* Hey, my name is Conor Bradfield. I have been developing for Tahste for some time. 
* Although I do a full time study at the University of the Sunshine coast with the Bachelor of Design (Game Design). 
* I’ve been passionate about game design ever since I was a child picking up the DS for the first time and knowing that this will be a passion of mine.
* I have created games in the past, a good example is Famed Kingdom uploaded onto itch.io.