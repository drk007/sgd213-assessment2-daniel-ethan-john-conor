# Desired Learning Outcomes

## What would Tahste like to learn from this Project?
* Further improving our coding ability in game engines such as Unity.
* Creating an immersive environment.
* Further improving teamwork and communication.
* Develop a better understanding of the process of developing a game.
* Build better relations with business partners and clients.
