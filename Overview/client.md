# About the Client

## The Client
* Michael is a part of a new upcoming indie game developer.
* He came to us with a proposal which they want us to start and they continue with the project further.
* His proposal is a simple horror type game.

## Intentions
* We had a chat to Michael about the game that he and his team want to develop.
* He said "We really like the classics, Slender man was a favourite which is why we want to replicate it."
* He also said "Dark wood also has nice aesthetic".

* In his proposal he also stated what mechanics he wanted to be involved.
* The first mechanic he stated was "The project would require enemy AI that would follow the player and jump out at them at certain points in the game."
* The second mechanic he wanted to be implimented into the game was "The player would need to move and interact with objects".