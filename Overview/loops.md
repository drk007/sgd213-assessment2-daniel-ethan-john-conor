# Game Loops

## Enemy Interaction
* Hear the enemy.
* Hide.
* Enemy leave.
* If didn't hide.
* Die.

## Object Pickups
* Pickup object when viewing.
* Object is placed in inventory.

## Environmental Distractions
* Run into certain objects (e.g. bushes, tall grass).
* Louder noise be created.
* Attracts big enemy.

## Environmental Interactions
* Interact with object when viewing.
* Object is then done doing its certain task (lever flips, door open).


