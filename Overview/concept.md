# High Concept

## Protagonist
* You are playing as a businessman, capable of doing the average tasks.

## Antagonist
* The enemy is a spirit of the forest is haunting the protagonist as he travels around the forest. 
* This primarily uses their hearing senses to detect where the protagonist is as the player has to try and hide and stay low to survive and traverse.

## The Objective
* You as the player are to find car parts to rebuild this abandoned car which will enable you to escape. 
* You will face many challenges along the way, one of them being that you NEED to stay alive.
## Background Info
* You will play as a working businessman who has fallen asleep on the subway train, as you wake up you find yourself in the train that has suddenly derailed and crashed. 
* Emerging from the subway station you find yourself in a forest at night.

## Introduction
* Emerging from the accident, you find yourself walking towards the stairs of the subway station. 
* Walking towards the top of the stairs, you exit the station and find yourself in a dark forest with eerie sounds echoing around you. 
* Only equipped with your phone, using it as a light source, you're left wandering throughout this forest.

## Items
* Rock
   * Can be used as distraction to defer the Antagonist.
* Phone
   * Used as a flashlight so player are able to see better but limited battery life
* Car Parts
   * These allow you to progress through the game and eventually allow you to escape using the abandoned car.

## Environment
* The game will be taken inside a forest with a few abandoned cabins and ruined cars littered across the map. 
* The game will start off in a ruined train.