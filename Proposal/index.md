# Project Overview
[//]: # (This section is an example structure for the proposal to your client.)

>This is all sample material in here, to give you an idea on how and what to populate each section with. If you think of other sections or a different document layout, please do use it!

## Design
[//]: # (How will you meet the client's brief, their expectations, and their requirements.)

### Project Name
Forest Lurker

### Description
[//]: # (This is the elevator pitch, sell the idea)
You wake up from a train crash and don't know where you are, you look up to realise that you are in a forest. In front of you is a dirt path that leads into the deep void of the woods. 
On your way down the path a cool breeze blows through the air and a lonely owl's hoot echoes in the distance, a branch crunching can be heard.

### Begin Date
Start of Task 2

### End Date
End of Task 3

### Justification
Read the design justification [here](project_justification.md).

---

## Statement of Works
[//]: # (This section is about managing expectations; list out all of the qualities that will be in the final product)

### Requirements
[//]: # (What are the requirements of the finished project?)
* Horror game based in a dark forest
* Project would require enemy AI that would follow the player and jump out at them at certain points in the game.
* The player would need to move and interact with objects
    * Needs a catchy name

### Expectations
[//]: # (What are the client's expectations?)
* Project delivered on time
* Weekly updates on progress
* Communication with the client when design issues encountered
* Quality transparent project management (add the client)

    * Can use basic geometry
    * Should still look nice using colour palettes

### Assumptions
[//]: # (What are you assuming based on client responses)
* Whole game to be placed in the dark forest
* The game has controls for the player to use

---

[//]: # (### Schedule of Rates)
[//]: # (This is where you would list your hourly rates and time estimations)

## Milestones
[//]: # (Breakdown of phases of development, with estimated delivery times)
[//]: # (In practice, if you were working on fixed price phases, you would also list expected payment after each phase.)
| Phase | Completion Date |
| --- | --- |
| Prototype | Week 13 |

---

## Agreement
[//]: # (List out the arrangement)
Work will be completed to fit the provided Statement of Works, any work outside of this arrangement will be billed at our hourly rate, or, quoted separately.

## Signatures
[//]: # (If dealing in person, agreements should be signed so that additional work can be billed)
| Client Name | Date | Signature |
| --- | --- | --- |
| Michael | DATE | ________ |

| Team Rep. | ID | Signature |
| --- | --- | --- |
| Daniel Kohn | 1147155 | ________ |
| --- | --- | --- |
| John Macale | 1148275  | ________ |
| --- | --- | --- |
| Conor Bradfield | 1143798  | ________ |
| --- | --- | --- |
| Ethan Mott | 1147420  | ________ |